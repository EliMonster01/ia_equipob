﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace ReconocimientoMatriculasCarro
{
    public class ProcesadorImagen
    {
        int[,] Pixeles;
        int[,] Binario;
        public static int[,] Etiquetada;
        Componente2 componenteConectada;
        int[,] mapaDeBits;
        int[,] Etiquetada2;
        int SumarIteraracion = 0;

        public ProcesadorImagen(Bitmap mapaDeBits)
        {
        //   this.mapaDeBits = new Bitmap(mapaDeBits.Width,mapaDeBits.Height,System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Pixeles = new int[mapaDeBits.Width, mapaDeBits.Height];

            for (int j=0; j<mapaDeBits.Height; j++)
            {
                for (int i=0; i< mapaDeBits.Width; i++)
                {
                    Pixeles[i, j] = mapaDeBits.GetPixel(i, j).ToArgb();
                }
            }
        }

        public void Binarizar(int umbral = 127)
        {
            int rojo, verde, azul, gris;
            Binario = new int[Pixeles.GetLength(0), Pixeles.GetLength(1)]; 

            for (int j=0; j<Pixeles.GetLength(1);j++)
            {
                for (int i=0; i<Pixeles.GetLength(0); i++)
                {
                    rojo = (Pixeles[i, j] & 0xff0000) >> 16;
                    verde = (Pixeles[i, j] & 0xff00) >> 8;
                    azul = Pixeles[i, j] & 0xff;
                    gris = (rojo + verde + azul) / 3;
                    if (gris > umbral)
                    {
                        Pixeles[i, j] = 255;
                        Binario[i, j] = 0;
                    }
                    else
                    {
                        Binario[i, j] = 1;
                        Pixeles[i, j] = 0;
                    }
                }
            }
        }
        
        public Bitmap GetMapaDeBitsBinario()
        {
            Bitmap mapa_Binario = new Bitmap (Pixeles.GetLength(0), Pixeles.GetLength(1), System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            for (int j=0; j<Pixeles.GetLength(1); j++)
            {
                for (int i=0; i<Pixeles.GetLength(0); i++)
                {
                    mapa_Binario.SetPixel(i,j,Color.FromArgb((255<<24) | (Pixeles[i,j] << 16) | (Pixeles[i,j] << 8) | Pixeles[i,j]));
                }
            }

            return mapa_Binario;
        }
        
        

        public void SumarEtiquetada()
        {
            Etiquetada = new int[Binario.GetLength(0), Binario.GetLength(1)];
            Etiquetada2 = new int[Binario.GetLength(0), Binario.GetLength(1)];
            int cont = 2;

            for (int j = 0; j < Pixeles.GetLength(1); j++)
            {
                for (int i = 0; i < Pixeles.GetLength(0); i++)
                {
                    if (Binario[i, j] == 1)
                    {
                        Etiquetada[i, j] = cont;
                        cont++;
                    }          
                }    
            }

            EtiquetaDeComponentesConexas();
        }

        public void AcomodarMatriz(int[,] etiqueta, int[,] etiquetaCopia)
        {
            for (int j = 0; j < Pixeles.GetLength(1); j++)
            {
                for (int i = 0; i < Pixeles.GetLength(0); i++)
                {
                    etiquetaCopia[i, j] = etiqueta[i, j];
                }
            }
        }
        public void EtiquetadaInversa()
        {            
            for (int j = Pixeles.GetLength(1)-1; j > 0; j--)
            {
                for (int i = Pixeles.GetLength(0)-1; i > 0; i--)
                {
                    if (Binario[i, j] == 1)
                    {
                        
                        if (i + 1 < Pixeles.GetLength(0) - 1)
                        {
                            if (Etiquetada[i, j] > Etiquetada[i + 1, j] && Etiquetada[i + 1, j] != 0)
                            {
                                Etiquetada[i, j] = Etiquetada[i + 1, j];
                            }
                        }

                        if (j + 1 < Pixeles.GetLength(1) - 1)
                        {
                             if (Etiquetada[i, j] > Etiquetada[i, j + 1] && Etiquetada[i, j + 1] != 0)
                            {
                                Etiquetada[i, j] = Etiquetada[i, j + 1];
                            }
                            
                        }

                        if (i - 1 >= 0)
                        {
                            if (Etiquetada[i, j] > Etiquetada[i - 1, j] && Etiquetada[i - 1, j] != 0)
                            {
                                Etiquetada[i, j] = Etiquetada[i - 1, j];
                            }
                        }

                        if (j - 1 >= 0)
                        {
                            if (Etiquetada[i, j] > Etiquetada[i, j - 1] && Etiquetada[i, j - 1] != 0)
                            {
                                Etiquetada[i, j] = Etiquetada[i, j - 1];
                            }
                        }
                        
                    }
                    
                }
              
            }
        }

        public void SegmentacionDeConexes()
        {
            componenteConectada = new Componente2();
            componenteConectada.Diccionario(Etiquetada);

        }
        public void EtiquetaDeComponentesConexas()
        {

            AcomodarMatriz(Etiquetada, Etiquetada2);

            for (int j = 0; j < Pixeles.GetLength(1); j++)
            {
                for (int i = 0; i < Pixeles.GetLength(0); i++)
                {
                    if (Binario[i, j] == 1)
                    {

                        if (i - 1 >= 0)
                        {
                            if (Etiquetada[i, j] > Etiquetada[i - 1, j] && Etiquetada[i - 1, j] != 0)
                            {
                                Etiquetada[i, j] = Etiquetada[i - 1, j];
                            }
                        }

                        if (j - 1 >= 0)
                        {
                            if (Etiquetada[i, j] > Etiquetada[i, j - 1] && Etiquetada[i, j - 1] != 0)
                            {
                                Etiquetada[i, j] = Etiquetada[i, j - 1];
                            }
                        }

                        if (i + 1 < Pixeles.GetLength(0) - 1)
                        {
                            if (Etiquetada[i, j] > Etiquetada[i + 1, j] && Etiquetada[i + 1, j] != 0)
                            {
                                Etiquetada[i, j] = Etiquetada[i + 1, j];
                            }
                        }

                        if (j + 1 < Pixeles.GetLength(1) - 1)
                        {
                            if (Etiquetada[i, j] > Etiquetada[i, j + 1] && Etiquetada[i, j + 1] != 0)
                            {
                                Etiquetada[i, j] = Etiquetada[i, j + 1];
                            }

                        }


                    }

                }

            }

            EtiquetadaInversa();

            if (SumarIteraracion < 1)
            {
                if (Etiquetada != Etiquetada2)
                {
                    SumarIteraracion++;
                    EtiquetaDeComponentesConexas();
                }
            }



        }

    }
}
