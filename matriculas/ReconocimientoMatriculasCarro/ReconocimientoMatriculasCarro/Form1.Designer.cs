﻿namespace ReconocimientoMatriculasCarro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edicionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.binarizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.etiquetarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.segmentarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OFD_Principal = new System.Windows.Forms.OpenFileDialog();
            this.PB_Principal = new System.Windows.Forms.PictureBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Principal)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.guardarToolStripMenuItem,
            this.edicionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(532, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem,
            this.guardarToolStripMenuItem1,
            this.salirToolStripMenuItem});
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.guardarToolStripMenuItem.Text = "Archivo";
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.abrirToolStripMenuItem.Text = "Abrir";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // guardarToolStripMenuItem1
            // 
            this.guardarToolStripMenuItem1.Name = "guardarToolStripMenuItem1";
            this.guardarToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.guardarToolStripMenuItem1.Text = "Guardar";
            this.guardarToolStripMenuItem1.Click += new System.EventHandler(this.guardarToolStripMenuItem1_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // edicionToolStripMenuItem
            // 
            this.edicionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.binarizarToolStripMenuItem,
            this.etiquetarToolStripMenuItem,
            this.segmentarToolStripMenuItem});
            this.edicionToolStripMenuItem.Name = "edicionToolStripMenuItem";
            this.edicionToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.edicionToolStripMenuItem.Text = "Edición";
            this.edicionToolStripMenuItem.Click += new System.EventHandler(this.binarizarToolStripMenuItem_Click);
            // 
            // binarizarToolStripMenuItem
            // 
            this.binarizarToolStripMenuItem.Name = "binarizarToolStripMenuItem";
            this.binarizarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.binarizarToolStripMenuItem.Text = "Binarizar";
            this.binarizarToolStripMenuItem.Click += new System.EventHandler(this.binarizarToolStripMenuItem_Click_1);
            // 
            // etiquetarToolStripMenuItem
            // 
            this.etiquetarToolStripMenuItem.Name = "etiquetarToolStripMenuItem";
            this.etiquetarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.etiquetarToolStripMenuItem.Text = "Etiquetar";
            this.etiquetarToolStripMenuItem.Click += new System.EventHandler(this.etiquetarToolStripMenuItem_Click);
            // 
            // segmentarToolStripMenuItem
            // 
            this.segmentarToolStripMenuItem.Name = "segmentarToolStripMenuItem";
            this.segmentarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.segmentarToolStripMenuItem.Text = "Segmentar";
            this.segmentarToolStripMenuItem.Click += new System.EventHandler(this.segmentarToolStripMenuItem_Click);
            // 
            // OFD_Principal
            // 
            this.OFD_Principal.FileName = "openFileDialog1";
            this.OFD_Principal.Filter = "Archivos PNG|*.png|Archivos JPG|*.jpg|Mapas de Bits|*.bmp|Todos los archivos|*.*";
            this.OFD_Principal.FileOk += new System.ComponentModel.CancelEventHandler(this.OFD_Principal_FileOk);
            // 
            // PB_Principal
            // 
            this.PB_Principal.Location = new System.Drawing.Point(0, 27);
            this.PB_Principal.Name = "PB_Principal";
            this.PB_Principal.Size = new System.Drawing.Size(532, 350);
            this.PB_Principal.TabIndex = 1;
            this.PB_Principal.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 376);
            this.Controls.Add(this.PB_Principal);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Principal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog OFD_Principal;
        private System.Windows.Forms.PictureBox PB_Principal;
        private System.Windows.Forms.ToolStripMenuItem edicionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem binarizarToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem etiquetarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem segmentarToolStripMenuItem;
    }
}

