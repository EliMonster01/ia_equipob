﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReconocimientoMatriculasCarro
{
    public partial class Form1 : Form
    {
        ProcesadorImagen procesador_imagen;

        public Form1()
        {
            InitializeComponent();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OFD_Principal.ShowDialog() == DialogResult.OK)
            {
                PB_Principal.Image = new Bitmap(OFD_Principal.FileName);
                procesador_imagen = new ProcesadorImagen((Bitmap) PB_Principal.Image);
            }
        }

        private void binarizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
           

        }

        private void binarizarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            procesador_imagen.Binarizar(127);
            PB_Principal.Image = procesador_imagen.GetMapaDeBitsBinario();
        }

        private void guardarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                PB_Principal.Image.Save(saveFileDialog.FileName + ".png");
            }
        }

        private void etiquetarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            procesador_imagen.SumarEtiquetada();
        }

        private void segmentarToolStripMenuItem_Click(object sender, EventArgs e)
        { 
            procesador_imagen.SegmentacionDeConexes();
        }

        private void OFD_Principal_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}
