﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ReconocimientoMatriculasCarro
{
    class Componente2
    {
        int XRigth, XLeft=ProcesadorImagen.Etiquetada.GetLength(0), YUp = ProcesadorImagen.Etiquetada.GetLength(1), YDown;
        int[,] pixeles;
        Dictionary<int, Componente2> Contenido = new Dictionary<int, Componente2>();

        public Componente2(int X, int Y)
        {
            XRigth = X;
            XLeft = X;
            YUp = Y;
            YDown= Y;
        }

        public Componente2()
        {

        }
        public void Diccionario(int[,] Etiquetada)
        {
            int id;
            Contenido.Add(0, new ReconocimientoMatriculasCarro.Componente2());

            for (int j = 0; j < Etiquetada.GetLength(1); j++)
            {
                for (int i = 0; i < Etiquetada.GetLength(0); i++)
                {
                    if (Etiquetada[i, j] != 0)
                    {
                        id = Etiquetada[i, j];

                        if (Contenido.ContainsKey(id))
                        {
                            Contenido[id].Direccion(i, j);
                        }
                        else
                        {
                            Contenido.Add(id, new Componente2(i, j));
                        }
                    }
                }
            }
            Imprimir();
            Replicar(Etiquetada);
        }


        public void Direccion(int posX, int posY)
        {
            if (posX  < XLeft)
            {
                XLeft = posX;
            }
            if (posX > XRigth)
            {
                XRigth = posX;
            }

            if (posY  < YUp)
            {
                YUp = posY;
            }

            if (posY > YDown)
            {
                YDown = posY;
            }
        }

        

        public void Imprimir()
        {
            for (int i=0; i<Contenido.Keys.Count;i++)
            {
                if (Contenido.Keys.ElementAt(i)!=0)
                {
                    Console.WriteLine(  Contenido.Keys.ElementAt(i) + Contenido[Contenido.Keys.ElementAt(i)].XRigth);
                    Console.WriteLine(  Contenido.Keys.ElementAt(i) + Contenido[Contenido.Keys.ElementAt(i)].XLeft);
                    Console.WriteLine(  Contenido.Keys.ElementAt(i) + Contenido[Contenido.Keys.ElementAt(i)].YUp);
                    Console.WriteLine(  Contenido.Keys.ElementAt(i) + Contenido[Contenido.Keys.ElementAt(i)].YDown);
                }
            }
        }

        public void Replicar(int[,]original)
        {

            for (int m=0; m<Contenido.Keys.Count;m++)
            {
                if (Contenido.Keys.ElementAt(m)!=0)
                {
                    int XRight = Contenido[Contenido.Keys.ElementAt(m)].XRigth;
                    int XLeft = Contenido[Contenido.Keys.ElementAt(m)].XLeft;
                    int YUp = Contenido[Contenido.Keys.ElementAt(m)].YUp;
                    int YDown = Contenido[Contenido.Keys.ElementAt(m)].YDown;
                    int DX = (XRight - XLeft) + 1;
                    int DY = (YDown - YUp) + 1;
                    if (DX > 7 && DY > 15)
                    {
                        if (DX/DY > 0.3)
                        {
                            Console.WriteLine(Contenido.Keys.ElementAt(m));
                            pixeles = new int[DX, DY];

                            for (int j = YUp; j < YDown; j++)
                            {
                                for (int i = XLeft; i < XRight; i++)
                                {
                                    pixeles[i - XLeft, j - YUp] = original[i, j];
                                    Console.Write(pixeles[i - XLeft, j - YUp] + " ");
                                }
                                Console.WriteLine(" ");
                            }
                        }  
                    }                                 
                }                
            }            
        }
    }
}
