﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace EjemploMonogame1
{
    class Fondos
    {
        public Texture2D dibujo;
        public Vector2 posicion = Vector2.Zero;
        public float profundidad = 0.0f;
        public float velocidad = 0.0f;
        public int direccion = -1; // de derecha a izquierda
        public Vector2 tamañodibujo = Vector2.Zero;
        public Color color = Color.White;
    }
    class MultiFondos
    {
        private bool moviendose = false;
        private bool moverHorizontal = true;
        private Vector2 Ventana;

        private List<Fondos> listafondos = new List<Fondos>();
        private SpriteBatch batch;

        public MultiFondos(GraphicsDeviceManager graficos)
        {
            Ventana.X = graficos.PreferredBackBufferWidth;
            Ventana.Y = graficos.PreferredBackBufferHeight;
            batch = new SpriteBatch(graficos.GraphicsDevice);
        }

        public void AdicionarFondo(Texture2D dibujo, float profundidad, float velocidad, int direccion)
        {
            Fondos fondo = new Fondos();
            fondo.dibujo = dibujo;
            fondo.profundidad = profundidad;
            fondo.velocidad = velocidad;
            fondo.tamañodibujo.X = dibujo.Width;
            fondo.tamañodibujo.Y = dibujo.Height;
            fondo.direccion = direccion;
            listafondos.Add(fondo);
        }

        public int CompararProfundidades(Fondos fondo1, Fondos fondo2)
        {
            if (fondo1.profundidad < fondo2.profundidad)
            {
                return 1;
            }
            if (fondo1.profundidad > fondo2.profundidad)
            {
                return -1;
            }
            if (fondo1.profundidad == fondo2.profundidad)
            {
                return 0;
            }
            return 0;
        }

        public void Mover(float velocidad)
        {
            float velocidadMovimiento = velocidad / 60.0f;
            foreach (Fondos fondo in listafondos)
            {
                float distanciaMovimiento = fondo.velocidad * velocidadMovimiento;
                if (!moviendose)
                {
                    if (moverHorizontal)
                    {
                        fondo.posicion.X += (distanciaMovimiento * fondo.direccion);
                        fondo.posicion.X = fondo.posicion.X % fondo.tamañodibujo.X;
                    }
                    else
                    {
                        fondo.posicion.Y += (distanciaMovimiento * fondo.direccion);
                        fondo.posicion.Y = fondo.posicion.Y % fondo.tamañodibujo.Y;
                    }
                }
            }
        }

        public void Dibujar()
        {
            listafondos.Sort(CompararProfundidades);
            batch.Begin();
            for (int i = 0; i < listafondos.Count; i++)
            {
                if (!moverHorizontal)
                {
                    if (listafondos[i].posicion.Y < Ventana.Y)
                    {
                        batch.Draw(listafondos[i].dibujo, new Vector2(0, listafondos[i].posicion.Y - listafondos[i].tamañodibujo.Y), listafondos[i].color);
                    }
                    else
                    {
                        batch.Draw(listafondos[i].dibujo, new Vector2(0, listafondos[i].posicion.Y + listafondos[i].tamañodibujo.Y), listafondos[i].color);
                    }
                }
                else
                {
                    if (listafondos[i].posicion.X < Ventana.X)
                    {
                        batch.Draw(listafondos[i].dibujo, new Vector2(listafondos[i].posicion.X, 0), listafondos[i].color);
                    }
                    if (listafondos[i].posicion.X > 0.0f)
                    {
                        batch.Draw(listafondos[i].dibujo, new Vector2(listafondos[i].posicion.X - listafondos[i].tamañodibujo.X, 0), listafondos[i].color);
                    }
                    else
                    {
                        batch.Draw(listafondos[i].dibujo, new Vector2(listafondos[i].posicion.X + listafondos[i].tamañodibujo.X, 0), listafondos[i].color);
                    }
                }
            }
            batch.End();
        }

        public void MoverVertical()
        {
            moverHorizontal = false;
        }
        public void MoverHorizontal()
        {
            moverHorizontal = true;
        }
        public void Parar()
        {
            moviendose = false;
        }
        public void IniciarMover()
        {
            moviendose = true;
        }
        public void SetPosicionFondo(int numeroFondo, Vector2 posicionIncial)
        {
            if (numeroFondo < 0 || numeroFondo >= listafondos.Count) return;
            listafondos[numeroFondo].posicion = posicionIncial;
        }
        public void SetTransparencia(int numberFondo, float porcentaje)
        {
            if (numberFondo < 0 || numberFondo >= listafondos.Count) return;
            float alpha = (porcentaje / 100.0f);
            listafondos[numberFondo].color = new Color(new Vector4(0.0f, 0.0f, 0.0f, alpha));

        }
        public void Actualizar(GameTime gameTime)
        {
            foreach (Fondos fondo in listafondos)
            {
                float distanciaMovimiento = fondo.velocidad / 60.0f;
                if (moviendose)
                {
                    if (moverHorizontal)
                    {
                        fondo.posicion.X += (distanciaMovimiento * fondo.direccion);
                        fondo.posicion.X = fondo.posicion.X % fondo.tamañodibujo.X;
                    }
                    else
                    {
                        fondo.posicion.Y += (distanciaMovimiento * fondo.direccion);
                        fondo.posicion.Y = fondo.posicion.Y % fondo.tamañodibujo.Y;
                    }
                }
            }
        }
    }

}