﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploMonogame1
{
    class EnemigoPerseguidorOrigen : Sprite
    {
        public EstadoPerseguidorPruebaEstados EstadoActual;

        public EnemigoPerseguidorOrigen()
        {
            EstadoActual = new EstadoPerseguiendoVictima(); 
        }

        public void Actualizate(Personaje victima, Sprite ZonaSegura)
        {
            EstadoActual.Procesate(this, victima, ZonaSegura);
        }

    }

    class EstadoPerseguidorPruebaEstados
    {
        public virtual void Procesate(EnemigoPerseguidorOrigen Llamador, Personaje victima, Sprite ZonaSegura)
        {
            
        }
    }

    class EstadoPerseguiendoVictima : EstadoPerseguidorPruebaEstados
    {
        public override void Procesate(EnemigoPerseguidorOrigen Llamador, Personaje victima, Sprite ZonaSegura)
        {
           

            if (victima.Posicion.X < Llamador.Posicion.X)
            {
                Llamador.Posicion.X -= Llamador.Velocidad.X;
            }
            if (victima.Posicion.X > Llamador.Posicion.X)
            {
                Llamador.Posicion.X += Llamador.Velocidad.X;
            }
            if (victima.Posicion.Y < Llamador.Posicion.Y)
            {
                Llamador.Posicion.Y -= Llamador.Velocidad.Y;
            }
            if (victima.Posicion.Y > Llamador.Posicion.Y)
            {
                Llamador.Posicion.Y += Llamador.Velocidad.Y;
            }


            if (victima.Colisionando(ZonaSegura))
            {
                Llamador.EstadoActual = new EstadoRegresandoAlOrigen();
            }
        }
    }


    class EstadoRegresandoAlOrigen : EstadoPerseguidorPruebaEstados
    {
        float PosicionOrigenX, PosicionOrigenY; 
        public override void Procesate(EnemigoPerseguidorOrigen Llamador, Personaje victima, Sprite ZonaSegura)
        {
           PosicionOrigenX = Llamador.PosicionInicial.X;
           PosicionOrigenY = Llamador.PosicionInicial.Y;

            if (PosicionOrigenX < Llamador.Posicion.X)
            {
                Llamador.Posicion.X -= Llamador.Velocidad.X;
            }
            if (PosicionOrigenX > Llamador.Posicion.X)
            {
                Llamador.Posicion.X += Llamador.Velocidad.X;
            }
            if (PosicionOrigenY < Llamador.Posicion.Y)
            {
                Llamador.Posicion.Y -= Llamador.Velocidad.Y;
            }
            if (PosicionOrigenY > Llamador.Posicion.Y)
            {
                Llamador.Posicion.Y += Llamador.Velocidad.Y;
            }


            if (!victima.Colisionando(ZonaSegura))
            {
                Llamador.EstadoActual = new EstadoPerseguiendoVictima();
            }
        }
    }
}
