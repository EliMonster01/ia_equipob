﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;

namespace EjemploMonogame1
{
 
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Sprite mi_sprite;
        Sprite ZonaSegura;
        Pelotita mi_pelotita;
        Personaje victima;
        List<EnemigoPerseguidor> persegidores;
        EnemigoPruebaEstados prueba_estados;
        EnemigoPerseguidorOrigen perseguidorEstado;

        

        List<EnemigoPerseguidor> perseguidor;

        MultiFondos fondo;

        Texture2D paisaje;

        public Game1()
        {
            

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

       
        protected override void Initialize()
        {
            fondo = new MultiFondos(graphics);

            mi_sprite = new Sprite();
            ZonaSegura = new Sprite();
            mi_pelotita = new Pelotita();
            victima = new Personaje();
          
            perseguidor = new List<EnemigoPerseguidor>();
            for(int i = 0; i < 10; i++)
            {
                perseguidor.Add(new EnemigoPerseguidor());
            }
            prueba_estados = new EnemigoPruebaEstados();
            perseguidorEstado = new EnemigoPerseguidorOrigen();

            base.Initialize();

        }

        protected override void LoadContent()
        {

     

            spriteBatch = new SpriteBatch(GraphicsDevice);

            mi_sprite.Inicializar(Content.Load<Texture2D>("prueba_asset_basico"), new Vector2(10, 100));
            ZonaSegura.Inicializar(Content.Load<Texture2D>("grande2"), new Vector2(0,180), new Vector2(0,0));
            perseguidorEstado.Inicializar(Content.Load<Texture2D>("mono enemigo3"), new Vector2(450, 205),  new Vector2(1, 1));
            victima.Inicializate(Content.Load<Texture2D>("troll2"), new Vector2(300,300), new Vector2(3,3));
 
            paisaje = Content.Load<Texture2D>("fondon");

            fondo.AdicionarFondo(paisaje, 0.5f, 80.0f, -1);
        }


        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            fondo.Mover(0.5f);
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            victima.Actualizate(Keyboard.GetState());

            perseguidorEstado.Actualizate(victima, ZonaSegura);


            base.Update(gameTime);
        }

        
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

     
            spriteBatch.Begin();
            fondo.Dibujar();
            ZonaSegura.Dibujate(spriteBatch);
            victima.Dibujate(spriteBatch);

            perseguidorEstado.Dibujate(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
