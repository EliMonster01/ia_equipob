﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EjemploMonogame1
{
    class EnemigoPruebaEstados : Sprite
    {
        public EstadoEnemigoPruebaEstados EstadoActual;

        public EnemigoPruebaEstados()
        {
            EstadoActual = new EstadoIrHaciaDerechaEnemigoPruebaEstado();
        }

        public void Actualizate()
        {
            EstadoActual.Procesate(this);
            
        }

    }

    class EstadoEnemigoPruebaEstados
    {
        public virtual void Procesate(EnemigoPruebaEstados Llamador)
        {

        }


    }

    class EstadoIrHaciaDerechaEnemigoPruebaEstado : EstadoEnemigoPruebaEstados
    {
        public override void Procesate(EnemigoPruebaEstados Llamador)
        {
            Llamador.Posicion.X += Llamador.Velocidad.X;

            if (Llamador.Posicion.X > 700)
            {
                Llamador.EstadoActual = new EstadoIrHaciaIzquierdaEnemigoPruebaEstado();
            }


        }
    }

    class EstadoIrHaciaIzquierdaEnemigoPruebaEstado : EstadoEnemigoPruebaEstados
    {
        public override void Procesate(EnemigoPruebaEstados Llamador)
        {
            Llamador.Posicion.X -= Llamador.Velocidad.X;

            if (Llamador.Posicion.X <=0)
            {
                Llamador.EstadoActual = new EstadoIrHaciaDerechaEnemigoPruebaEstado();
            }


        }

    }








}
