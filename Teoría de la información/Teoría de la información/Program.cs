﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teoría_de_la_información
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduzca la información: ");
            string entrada = "";
            entrada = Console.ReadLine();

            double entropia = 0;
            Dictionary<char, double> tabla = new Dictionary<char, double>();

            foreach (char c in entrada)
            {
                if (tabla.ContainsKey(c))
                    tabla[c]++;
                else
                    tabla.Add(c, 1);

                Console.WriteLine("\n" + c);
            }

            double frecuencia;
            foreach (KeyValuePair<char, double> letra in tabla)
            {
                frecuencia = letra.Value / entrada.Length;
                entropia += frecuencia * (Math.Log(frecuencia)/Math.Log(2));
            }
            entropia *= -1;


            Console.WriteLine("\n" + "ENTROPÍA DE SHANNON: \n" + entropia.ToString());
        
            Console.ReadLine();
        }
    }
}
