﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioPerceptron
{
    public class RedNeuronal
    {
        public List<Capas> listaCapas = new List<Capas>();
        Capas capa;
        LeerArchivoCSV leerArch = new LeerArchivoCSV();
        int capasIngresadas, contarCapa = 0;
        string opc;
        bool bandera = true;
        double[] perceptronArray;

        public RedNeuronal()
        {
            perceptronArray = leerArch.leerAchivoCSV("..\\..\\CarpetaArchivos\\PerceptronMulticapa.csv");
            Console.WriteLine("\n----------  Bienvenido al sistema de Perceptrón Multicapa   ---------");
    
            Console.WriteLine("\n¿Cuántas Capas de la red desea utilizar?");
            capasIngresadas = int.Parse(Console.ReadLine());

            for (int i = 0; i < capasIngresadas; i++)
            {

                if (i == 0)
                {
                    capa = new Capas();
                }
                else
                {
                    capa = new Capas(contarCapa, listaCapas[contarCapa - 1].listaPerceptrones.Count);
                }
                contarCapa++;
                listaCapas.Add(capa);
            }
        }

        public void ejecutarRed()
        {
            while (bandera)
            {
                int contador = 0;
                Console.WriteLine("¿Cuál función de activación que desea realizar?");
                Console.WriteLine("\n1) Hardlim\n2) Hardlims");
                opc = Console.ReadLine();
                switch (opc)
                {
                    case "1":
                        foreach (Capas capas in listaCapas)
                        {
                            capas.PHardlim(perceptronArray);
                            capas.mostrarResultados(contador + 1);
                            actualizarP(capas, listaCapas, contador);
                            contador++;
                        }
                        actualizarRed();
                        break;

                    case "2":
                        foreach (Capas capas in listaCapas)
                        {
                            capas.PHardlims(perceptronArray);
                            capas.mostrarResultados(contador + 1);
                            actualizarP(capas, listaCapas, contador);
                            contador++;
                        }
                        actualizarRed();
                        break;
                }
            }
        }

        public void mostrarPe(double[] P)
        {
            for (int i = 0; i < P.Length; i++)
            {
                Console.WriteLine(P[i]);
            }
            Console.ReadLine();
        }

        public void mostrarElementosEnCapas()
        {
            int numerodecapa = 1;
            foreach (Capas capa in listaCapas)
            {
                int contadorP = 1;
                Console.WriteLine("---------- Datos de perceptron de la capa " + numerodecapa + " ----------");
                foreach (ClasePerceptron perceptron in capa.listaPerceptrones)
                {
                    Console.WriteLine("Perceptron " + contadorP);
                    Console.Write("W: ");
                    for (int i = 0; i < perceptron.W.Length; i++)
                    {
                        Console.Write(perceptron.W[i] + " ");
                    }
                    Console.WriteLine("\nBias: " + perceptron.bias + "\n");
                    contadorP++;
                }
                numerodecapa++;
            }

            Console.ReadLine();
        }

        public void actualizarP(Capas capa, List<Capas> ListaCapas, int indicador)
        {
            perceptronArray = capa.Finales;
        }

        public void actualizarRed()
        {
            Console.WriteLine("\n--------- ¿Ahora qué deseas realizar? --------------");
            Console.WriteLine("\n1)Agregar una nueva capa\n2)Agregar un nuevo perceptron\n3) Salir del sistema");
            switch (Console.ReadLine())
            {
                case "1":

                    if (listaCapas[listaCapas.Count - 1].listaPerceptrones.Count < perceptronArray.Length)
                    {
                        Console.WriteLine("Error.\nNo es posible agregar más capas.");
                    }
                    else
                    {
                        int numEnt = listaCapas[listaCapas.Count - 1].listaPerceptrones.Count;
                        capa = new Capas(listaCapas.Count, numEnt);
                        listaCapas.Add(capa);
                        //mostrarObjetosCapas();
                    }
                    break;

                case "2":
                    Console.WriteLine("\n---------- Opción Perceptron Nuevo --------------");
                    Console.WriteLine("Ingrese el numero de capa: ");
                    int numCapa = int.Parse(Console.ReadLine());
                    double[] W = new double[listaCapas[numCapa - 1].listaPerceptrones[0].W.Length];

                    Console.WriteLine("Ingrese el Bias nuevo: ");
                    double biasNuevo = int.Parse(Console.ReadLine());

                    ClasePerceptron percepetronNuevo = new ClasePerceptron(W, biasNuevo);
                    listaCapas[numCapa - 1].listaPerceptrones.Add(percepetronNuevo);
                    listaCapas[numCapa - 1].Finales = new double[listaCapas[numCapa - 1].listaPerceptrones.Count];
                    //mostrarObjetosCapas();
                    break;

                case "3":
                    bandera = false;
                    break;

                default:
                    Console.WriteLine("Error.\nUtilice una opción válida --> (1-3)");
                    break;

            }
        }
    }
}
