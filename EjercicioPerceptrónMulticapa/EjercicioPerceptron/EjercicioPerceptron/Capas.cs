﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioPerceptron
{
    public class Capas
    {
        public List<ClasePerceptron> listaPerceptrones = new List<ClasePerceptron>();
        public ClasePerceptron perceptron;
        public double[] Finales;
        private int perceptronesIngresados, pesos, contadorPerceptron = 1;
        private double peso;
        private double bias;


        public Capas()
        {
            Console.Write("Ingrese la cantidad de perceptrones en la Capa 1: ");
            perceptronesIngresados = int.Parse(Console.ReadLine());
            Finales = new double[perceptronesIngresados];

            Console.Write("Ingrese la cantidad de entradas de perceptrones: ");
            pesos = int.Parse(Console.ReadLine());

            for (int i = 0; i < perceptronesIngresados; i++)
            {
                double[] W = new double[pesos];
                Console.WriteLine("\n------Datos del perceptrón " + (i+1) + "------------" );
                for (int j = 1; j <= pesos; j++)
                {
                    Console.WriteLine("Ingrese el peso de la entrada " + j + ": ");
                    peso = double.Parse(Console.ReadLine());
                    W[j - 1] = peso;
                }

                Console.WriteLine("Ingrese el valor del Bias: ");
                bias = double.Parse(Console.ReadLine());
                perceptron = new ClasePerceptron(W, bias);
                listaPerceptrones.Add(perceptron);
                contadorPerceptron++;

                Console.WriteLine("");
            }

        }

        public Capas(int numeroCapa, int entradas)
        {
            Console.Write("Ingrese cantidad de perceptrones en la capa [" + (numeroCapa + 1) + "]: ");
            perceptronesIngresados = int.Parse(Console.ReadLine());
            Finales = new double[perceptronesIngresados];


            for (int i = 0; i < perceptronesIngresados; i++)
            {
                double[] W = new double[entradas];
                Console.WriteLine("\n-----------Datos del perceptrón " + (i+1)+ "-----------");
                for (int j = 1; j <= entradas; j++)
                {
                    Console.WriteLine("Ingrese el peso de la entrada " + j + ": ");
                    peso = double.Parse(Console.ReadLine());
                    W[j - 1] = peso;
                }

                Console.WriteLine("Ingrese el valor del Bias: ");
                bias = double.Parse(Console.ReadLine());
                perceptron = new ClasePerceptron(W, bias);
                listaPerceptrones.Add(perceptron);
                contadorPerceptron++;

                Console.WriteLine("");
            }

        }

        public void PHardlim(double[] P)
        {
            int contEntradas = 0;
            foreach (ClasePerceptron p in listaPerceptrones)
            {
                p.entrenarPerceptronHardlim(P);
                p.guardarDatos(Finales, contEntradas);
                contEntradas++;
            }

        }

        public void PHardlims(double[] P)
        {
            int contadorEntradas = 0;
            foreach (ClasePerceptron p in listaPerceptrones)
            {
                p.entrenarPerceptronHardlims(P);
                p.guardarDatos(Finales, contadorEntradas);
                contadorEntradas++;
            }
        }

        public void mostrarResultados(int contador)
        {
            Console.WriteLine("\nLa Salida (T) de  la capa [" + (contador) + "] es: ");
            for (int i = 0; i < Finales.Length; i++)
            {
                Console.Write(Finales[i]);
            }
        }
    }
}
