﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioPerceptron
{
    public class LeerArchivoCSV
    {
        public List<double[]> datosPerc = new List<double[]>();

        public double[] leerAchivoCSV(string path)
        {
            using (StreamReader lector = new StreamReader(path))
            {
                double[] perceptronDato = new double[1];
                string linea;
                string[] fila;

                while ((linea = lector.ReadLine()) != null)
                {
                    fila = linea.Split(',');
                    perceptronDato = new double[fila.Length];

                    for (int i = 0; i < fila.Length; i++)
                    {
                        perceptronDato[i] = double.Parse(fila[i]);
                    }

                }
                return perceptronDato;
            }
        }
    }
}
