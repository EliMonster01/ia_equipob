﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioPerceptron
{
    public class ClasePerceptron
    {
        public double[] W;
        public double N, A, bias;
        public bool bandera;

        public ClasePerceptron(int cantidadEntradas, double rangoInicial, double rangoFinal)
        {
            Random r = new Random();

            for (int i = 0; i < cantidadEntradas; i++)
            {
                W[i] = r.Next(int.Parse(rangoInicial.ToString()), int.Parse(rangoFinal.ToString()));
            }

            bias = r.Next(int.Parse(rangoInicial.ToString()), int.Parse(rangoFinal.ToString()));
        }

        public ClasePerceptron(double[] W, double bias)
        {
            this.W = W;
            this.bias = bias;
        }

        public void entrenarPerceptronHardlims(double[] Per)
        {
            double T = Per[Per.Length - 1];
            evaluarRed(Per);
            evaluarHardlims();

        }

        public void entrenarPerceptronHardlim(double[] Per)
        {
            double T = Per[Per.Length - 1];
            evaluarRed(Per);
            evaluarHardlim();

        }

        public void evaluarRed(double[] P)
        {
            N = bias;
            for (int i = 0; i < P.Length - 1; i++)
            {
                N += W[i] * P[i];
            }
        }

        public void evaluarHardlims()
        {
            A = N < 0 ? -1 : 1;
        }

        public void evaluarHardlim()
        {
            A = N < 0 ? 0 : 1;
        }

        public void ajustePesos(double[] P)
        {
            double E = P[P.Length - 1] - A;
            Console.WriteLine("Error: " + E);

            for (int i = 0; i < P.Length - 1; i++)
            {
                W[i] += E * P[i];
            }

            bias += E;

            mostrarDatos();
        }

        public void mostrarDatos()
        {
            for (int i = 0; i < W.Length; i++)
            {
                Console.WriteLine("L" + (i + 1) + ": " + W[i]);
            }
            Console.WriteLine("Bias: " + bias + "\n");
        }

        public void guardarDatos(double[] salidas, int x)
        {
            salidas[x] = A;
        }
    }
}
