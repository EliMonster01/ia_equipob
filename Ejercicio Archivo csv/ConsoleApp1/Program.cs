﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lineas = File.ReadAllLines("./Files/Archivo.csv");
            string[] datos = File.ReadAllLines("./Files/c1c2.csv");
            double x1, x2, y1, y2;

            foreach (var linea in lineas)
            {
                double res1, res2, resf;

                foreach (var dato in datos)
                {
                    var valores = linea.Split(',');
                    var dada = dato.Split(',');
                    x1 = double.Parse(valores[1]);
                    y1 = double.Parse(valores[2]);
                    x2 = double.Parse(dada[1]);
                    y2 = double.Parse(dada[2]);
                    
                    res1 = x1 - x2;
                    res2 = y1 - y2;
                    res1 = res1 * res1;
                    res2 = res2 * res2;
                    resf = Math.Sqrt(res1+res2);
                    Console.WriteLine("La distancia entre " + valores[0] + " y " + dada[0] +
                        " es: " + resf);
                }

                
                Console.ReadLine();
            }
        }
    }
}
