﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace EjemploMonogame1
{
    class Personaje : Sprite
    {
      
        public void Actualizar(KeyboardState Teclado)
        {
            if (Teclado.IsKeyDown(Keys.Right))
            {
                Posicion.X += Velocidad.X;
            }
            if (Teclado.IsKeyDown(Keys.Left))
            {
                Posicion.X -= Velocidad.X;
            }
            if (Teclado.IsKeyDown(Keys.Down))
            {
                Posicion.Y += Velocidad.Y;
            }
            if (Teclado.IsKeyDown(Keys.Up))
            {
                Posicion.Y -= Velocidad.Y;
            }
        }
        
    }
}
