﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EjemploMonogame1
{
    class EnemigoPruebaEstados:Sprite
    {
        public EstadoEnemigoPruebaEstados EstadoActual;

        public EnemigoPruebaEstados()
        {
        }

        public void Actualizate()
        {
            EstadoActual.Procesate(this);
        }
    }
    class EstadoEnemigoPruebaEstados
    {
        public virtual void Procesate(EnemigoPruebaEstados llamador)
        {

        }
    }
    class EstadoIrHaciaDerechaEnemigoPruebaEstados :EstadoEnemigoPruebaEstados
    {
        public override void Procesate(EnemigoPruebaEstados llamador)
        {
            llamador.Posicion.X += llamador.Velocidad.X;
            if (llamador.Posicion.X > 600)
            {
                llamador.EstadoActual = new EstadoIrHaciaIzquierdaEnemigoPruebaEstados();
            }
        }
    }
    class EstadoIrHaciaIzquierdaEnemigoPruebaEstados : EstadoEnemigoPruebaEstados
    {
        public override void Procesate(EnemigoPruebaEstados llamador)
        {
            llamador.Posicion.Y -= llamador.Velocidad.Y;
            if (llamador.Posicion.Y > 0)
            {
                llamador.EstadoActual = new EstadoIrHaciaDerechaEnemigoPruebaEstados();
            }
        }
    }
}
