﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace EjemploMonogame1
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
    
        Personaje protagonista;
        EnemigoPerseguidor enemigo;

        MultiFondos fondo;

        Texture2D paisaje;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            fondo = new MultiFondos(graphics);
            protagonista = new Personaje();
            enemigo = new EnemigoPerseguidor();
   
            base.Initialize();

        }

        protected override void LoadContent()
        {
    
            spriteBatch = new SpriteBatch(GraphicsDevice);
           
            protagonista.Inicializar(Content.Load<Texture2D>("gato2"), new Vector2(300, 300), new Vector2(3, 3));
            enemigo.Inicializar(Content.Load<Texture2D>("mono enemigo2"), new Vector2(0, 0), new Vector2(1, 1), new Vector2(0.1f, 0.1f));
            paisaje = Content.Load<Texture2D>("fondo"); 
            
            fondo.AdicionarFondo(paisaje, 0.5f, 80.0f, -1);
        }

        
        protected override void UnloadContent()
        {
    
        }

        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            fondo.Mover(0.5f);
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            protagonista.Actualizar(Keyboard.GetState());
            enemigo.Actualizar(protagonista);
            if (protagonista.Colisionando(enemigo) == true)
                protagonista.Inicializar(Content.Load<Texture2D>("explosion"), new Vector2(100, 100), new Vector2(5, 5)); //debe cambiar de forma
        
            base.Update(gameTime);
        }

        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

     
            spriteBatch.Begin();
            fondo.Dibujar();
            protagonista.Dibujate(spriteBatch);
            enemigo.Dibujate(spriteBatch);
        
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
