﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EjemploMonogame1
{
    class EnemigoPerseguidor : Sprite
    {
       
        public void Actualizar(Sprite Personaje)
        {

            if(Personaje.Posicion.X < Posicion.X)
            {
                Velocidad.X -= Aceleracion.X;
            }
            if (Personaje.Posicion.X > Posicion.X)
            {
                Velocidad.X += Aceleracion.X;
            }
            if (Personaje.Posicion.Y < Posicion.Y)
            {
                Velocidad.Y -= Aceleracion.Y;
            }
            if (Personaje.Posicion.Y > Posicion.Y)
            {
                Velocidad.Y += Aceleracion.Y;
            }

            if (Velocidad.X > 2)
                Velocidad.X = 2;
            if (Velocidad.X < -2)
                Velocidad.X = -2;
            if (Velocidad.Y < -2)
                Velocidad.Y = -2;
            if (Velocidad.Y > 2)
                Velocidad.Y = 2;
            


            Posicion.X += Velocidad.X;
            Posicion.Y += Velocidad.Y;
        }


    }
}
