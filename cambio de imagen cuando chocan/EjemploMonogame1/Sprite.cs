﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace EjemploMonogame1
{
    class Sprite
    {
        public Vector2 Posicion;
        public Texture2D Grafico;
        public Vector2 Velocidad;
        public Vector2 Aceleracion;

        public void Inicializar(Texture2D Grafico, Vector2 Posicion, Vector2 Velocidad, Vector2 Aceleracion)
        {
            this.Grafico = Grafico;
            this.Posicion = Posicion;
            this.Velocidad = Velocidad;
            this.Aceleracion = Aceleracion;
        }

        public void Inicializar(Texture2D Grafico, Vector2 Posicion, Vector2 Velocidad)
        {
            this.Grafico = Grafico;
            this.Posicion = Posicion;
            this.Velocidad = Velocidad;
        }

       

        public void Inicializar(Texture2D Grafico,Vector2 Posicion)
        {
            this.Grafico = Grafico;
            this.Posicion = Posicion;
        }

        public void Dibujate(SpriteBatch Dibujador)
        {
            Dibujador.Draw(Grafico, Posicion, Color.White);
        }

        public void Actualizate()
        {
            Posicion.X--;
        }


        public bool Colisionando(Sprite P)
        {
       
            
            if (Posicion.X > (P.Posicion.X + P.Grafico.Width))
            {
                return false;
            }
            else if ((Posicion.X + Grafico.Width) < P.Posicion.X)
            {
                return false;
            }
            else if (Posicion.Y > (P.Posicion.Y + P.Grafico.Height))
            {
                return false;
            }
            else if ((Posicion.Y +  Grafico.Height) < P.Posicion.Y)
            {
                return false;
            }

            return true;
        }
    }
}
