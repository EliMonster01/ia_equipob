﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EjemploMonoGame2
{
   public class Sprite 
    {
        public Vector2 PosicionAbsoluta;
        public Vector2 PosicionRelativa;
        public Vector2 Velocidad;
        public Texture2D Grafico;

        public void Inicializar(Texture2D Grafico, Vector2 PosicionAbsoluta, Vector2 Velocidad)
        {
            this.Grafico = Grafico;
            this.PosicionAbsoluta = PosicionAbsoluta;
            this.Velocidad = Velocidad;
        }

        public void Procesate()
        {
            PosicionRelativa = PosicionAbsoluta - DatosJuego.Jugador.PosicionAbsoluta; 
        }

        public void Dibujate()
        {
            DatosJuego.Dibujador.Draw(Grafico, PosicionRelativa, Color.White);
        }
    }
}
