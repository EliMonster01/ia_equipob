﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace EjemploMonoGame2
{
    class Jugador : Sprite
    {
        public void Procesate(KeyboardState Teclado)
        {
            if (Teclado.IsKeyDown(Keys.Right))
            {
                PosicionAbsoluta.X += Velocidad.X;
            }
            if (Teclado.IsKeyDown(Keys.Left))
            {
                PosicionAbsoluta.X -= Velocidad.X;
            }
            if (Teclado.IsKeyDown(Keys.Down))
            {
                PosicionAbsoluta.Y += Velocidad.Y;
            }
            if (Teclado.IsKeyDown(Keys.Up))
            {
                PosicionAbsoluta.Y -= Velocidad.Y;
            }
        }

        public void dibujate()
        {
            Vector2 posicionDibujado = new Vector2(400, 200); // Lo dibuja siempre en donde mismo 
            DatosJuego.Dibujador.Draw(Grafico, posicionDibujado, Color.White);
        }
    }
}
