﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EjemploMonoGame2
{
    class Tile : Sprite 
    {
        Vector2 PosicionTile;
        public int TipoDeTile;
        public int ValordeMov; 

        public const int ANCHURA_TILE = 32;
        public const int ALTURA_TILE = 32;

        public const int TIPO_TILE_AGUA = 0;
        public const int TIPO_TILE_PASTO = 1;
        public const int TIPO_TILE_PARED = 2;

        public void Inicializar(Texture2D Grafico, Vector2 PosicionTile, int TipoDeTile)
        {
            this.Grafico = Grafico;
            this.PosicionTile = PosicionTile;
            this.TipoDeTile= TipoDeTile;
            PosicionAbsoluta = new Vector2(PosicionTile.X * ANCHURA_TILE, PosicionTile.Y * ALTURA_TILE);
        }
    }
}
