﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;

namespace EjemploMonoGame2
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        List<Sprite> sprites;
        Jugador jugador;
        Tile[,] tiles; 

        string[] lineasArchivo = File.ReadAllLines("C:/Users/lumac/source/repos/EjemploMonoGame2/EjemploMonoGame2/Archivos/tiles.csv.csv");

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            sprites = new List<Sprite>();
            for (int i = 0; i < 10; i++)
            {
                sprites.Add(new Sprite());
            }

            jugador = new Jugador();

            int contador = 0;
            for (int i = 0; i < lineasArchivo.Length; i++)
            {
                contador = 0;
                var valor_tile = lineasArchivo[i].Split(',');
                for (int j = 0; j < valor_tile.Length; j++)
                {
                    contador++;
                }
            }

            tiles = new Tile[contador,lineasArchivo.Length];
            for (int i = 0; i < tiles.GetLength(1); i++)
            {
                for (int j = 0; j < tiles.GetLength(0); j++)
                {
                    tiles[j, i] = new Tile();
                }
            }
            base.Initialize();
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            DatosJuego.Dibujador = spriteBatch;
            DatosJuego.Jugador = jugador;

            for (int i = 0; i < sprites.Count; i++)
            {
                sprites[i].Inicializar(Content.Load<Texture2D>("sprite1"), new Vector2(i*200, 300), new Vector2 (0, 0)); 
            }

            jugador.Inicializar(Content.Load<Texture2D>("sprite2"), new Vector2(100, 100), new Vector2(5, 5));

            Texture2D grafico_tile_pasto = Content.Load<Texture2D>("tile_pasto");
            Texture2D grafico_tile_agua = Content.Load<Texture2D>("tile_agua");
            Texture2D grafico_tile_pared = Content.Load<Texture2D>("tile_pared");

            for (int i = 0; i < tiles.GetLength(1); i++)
            {
                var valores = lineasArchivo[i].Split(',');
                for (int j = 0; j < tiles.GetLength(0); j++)
                {
                    if (Int32.Parse(valores[j]) == 1)
                    {
                        tiles[j, i].Inicializar(grafico_tile_pasto, new Vector2(j, i), Tile.TIPO_TILE_PASTO);
                    }
                    if (Int32.Parse(valores[j]) == 0)
                    {
                        tiles[j, i].Inicializar(grafico_tile_agua, new Vector2(j, i), Tile.TIPO_TILE_AGUA);
                    }
                    if (Int32.Parse(valores[j]) == 2)
                    {
                        tiles[j, i].Inicializar(grafico_tile_pared, new Vector2(j, i), Tile.TIPO_TILE_PARED);
                    }
                    
                }
            }

            
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            jugador.Procesate(Keyboard.GetState());
            for (int i = 0; i < sprites.Count; i++)
            {
                sprites[i].Procesate();
            }

            for (int i = 0; i < tiles.GetLength(1); i++)
            {
                for (int j = 0; j < tiles.GetLength(0); j++)
                {
                    tiles[j, i].Procesate();
                }
            }


            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            DatosJuego.Dibujador.Begin();

            //for (int i = 0; i < sprites.Count; i++)
            //{
            //    sprites[i].Dibujate();
            //}

            for (int j = 0; j < tiles.GetLength(1); j++)
            {
                for (int i = 0; i < tiles.GetLength(0); i++)
                {
                    tiles[i, j].Dibujate();
                }
            }

            //for (int j = 0; j < tiles.GetLength(1); j++)
            //{
            //    for (int i = 0; i < tiles.GetLength(0); i++)
            //    {
            //        tiles[i, j].Dibujate();
            //    }
            //}

            jugador.dibujate();
            DatosJuego.Dibujador.End();

            
            // TODO: Add your drawing code here
        
            base.Draw(gameTime);
        }
    }
}
