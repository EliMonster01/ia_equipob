﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] lineas = File.ReadAllLines("./Files/Archivo.csv");

            foreach (var linea in lineas)
            {
                var valores = linea.Split(',');
                //Console.WriteLine("Punto: " + valores[0] + ", Valor en el Dato1 de: " + valores[1] 
                //    + ", Valor en el Dato2 de: " + valores[2]);

                //int x;
                //valores[1] = int.Parse(x);
                int a = int.Parse(valores[1].ToString());
                int b = int.Parse(valores[2].ToString());
                int c = int.Parse(valores[4].ToString());
                int d = int.Parse(valores[5].ToString());
                int e = int.Parse(valores[7].ToString());
                int f = int.Parse(valores[8].ToString());
                int g = int.Parse(valores[10].ToString());
                int h = int.Parse(valores[11].ToString());
                int i = int.Parse(valores[13].ToString());
                int j = int.Parse(valores[14].ToString());
                int k = int.Parse(valores[16].ToString());
                int l = int.Parse(valores[17].ToString());
                int m = int.Parse(valores[19].ToString());
                int n = int.Parse(valores[20].ToString());

                //int x;
                //x = a + b;
                //Console.WriteLine(x);



                int res = 0;
                do
                {
                    Console.WriteLine("------MENÚ DE OPCIONES---------");
                    Console.WriteLine("¿Qué distancia quieres obtener de los datos?\n" + "1)Distancia de los puntos a C1\n2)Distancia de los puntos a C2\n3)Trabajar con mas dimensiones\n4)Salir");
                    //String res = null;


                    res = Convert.ToInt16(Console.ReadLine());
                    //res = Console.ReadKey().ToString();
                    switch (res)
                    {
                        case 1: // Case "1" cuando se trabaja con string
                            Console.WriteLine("------------Opcion distancias de Puntos a C1----------");

                            Console.WriteLine("\nDistancia de A a C1: " + (Math.Sqrt(Math.Pow((a - k), 2)) + Math.Sqrt(Math.Pow((b - l), 2))));
                            Console.WriteLine("Distancia de B a C1: " + (Math.Sqrt(Math.Pow((c - k), 2)) + Math.Sqrt(Math.Pow((d - l), 2))));
                            Console.WriteLine("Distancia de C a C1: " + (Math.Sqrt(Math.Pow((e - k), 2)) + Math.Sqrt(Math.Pow((f - l), 2))));
                            Console.WriteLine("Distancia de D a C1: " + (Math.Sqrt(Math.Pow((g - k), 2)) + Math.Sqrt(Math.Pow((h - l), 2))));
                            Console.WriteLine("Distancia de E a C1: " + (Math.Sqrt(Math.Pow((i - k), 2)) + Math.Sqrt(Math.Pow((j - l), 2))));
                            break;

                        case 2:
                            Console.WriteLine("------------Opcion distancias de Puntos a C2----------");

                            Console.WriteLine("\nDistancia de A a C2:" + (Math.Sqrt(Math.Pow((a - m), 2)) + Math.Sqrt(Math.Pow((b - n), 2))));
                            Console.WriteLine("Distancia de B a C2: " + (Math.Sqrt(Math.Pow((c - m), 2)) + Math.Sqrt(Math.Pow((d - n), 2))));
                            Console.WriteLine("Distancia de C a C2: " + (Math.Sqrt(Math.Pow((e - m), 2)) + Math.Sqrt(Math.Pow((f - n), 2))));
                            Console.WriteLine("Distancia de D a C2: " + (Math.Sqrt(Math.Pow((g - m), 2)) + Math.Sqrt(Math.Pow((h - n), 2))));
                            Console.WriteLine("Distancia de E a C2: " + (Math.Sqrt(Math.Pow((i - m), 2)) + Math.Sqrt(Math.Pow((j - n), 2))));

                            break;
                        case 3:
                            Console.WriteLine("Programa finalizado.");
                            break;
                        default:
                            Console.WriteLine("No ha seleccionado ninguna opción");
                            break;

                    }
                    Console.ReadKey();
                } while (res != 3);


                Console.ReadLine();

            }

        }
    }
}
