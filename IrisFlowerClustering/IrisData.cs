﻿
using Microsoft.ML.Data;

namespace ProgramaIrisClustering
{
    // Definiendo columnas
    public class IrisData
    {
        [LoadColumn(0)]
        public float SepalLength;

        [LoadColumn(1)]
        public float SepalWidth;

        [LoadColumn(2)]
        public float PetalLength;

        [LoadColumn(3)]
        public float PetalWidth;
    }

    public class ClusterPrediction
    {
        [ColumnName("PredictedLabel")] //ID del cluster a predecir
        public uint PredecidoClusterId;

        [ColumnName("Score")] //array con resultados euclideanos para calcular distancias con el cluster del centroide
        public float[] Distancias;
        
    }
}
