﻿// Librerias para el sdk
using System;
using System.IO;
using Microsoft.ML;
using Microsoft.ML.Data;

namespace ProgramaIrisClustering
{
    class Program
    {
        // Creando los caminos (Paths) para su deteccion utilizando sdk .net version 2.2
        static readonly string caminoData = Path.Combine(Environment.CurrentDirectory, "Data", "iris.data"); //contiene la ruta al archivo para entrenar el modelo
        
        static readonly string modeloCamino = Path.Combine(Environment.CurrentDirectory, "Data", "IrisClusteringModel.zip"); //almacena modelo entrenado

        static void Main(string[] args)
        {
            //El contexto ML se comparte entre los objetos de flujo de trabajo de creación de modelos 
            var mlContexto = new MLContext(seed: 0);


            // 1. Cargar archivo Iris.data
            Console.WriteLine($"--------- Espere mientras se analizan los clusters------------");
            IDataView dataView = mlContexto.Data.LoadFromTextFile<IrisData>(caminoData, hasHeader: false, separatorChar: ',');

            

            // 2.Creando el pipeline.
            string featuresNombreColumna = "Features";
            var pipeline = mlContexto.Transforms
                .Concatenate(featuresNombreColumna, "SepalLength", "SepalWidth", "PetalLength", "PetalWidth")
                .Append(mlContexto.Clustering.Trainers.KMeans(featuresNombreColumna, numberOfClusters: 3));
            

            // 3. Entrena el modelo para almacenar
            var modelo = pipeline.Fit(dataView);

            // 4: Guardando el modelo en el zip
            using (var fileStream = new FileStream(modeloCamino, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                mlContexto.Model.Save(modelo, dataView.Schema, fileStream);
            }

            // 5. Usando el modelo para predicciones
            var prediceModelo = mlContexto.Model.CreatePredictionEngine<IrisData, ClusterPrediction>(modelo);

            // Imprimiendo el cluster y sus distancias en Iris:
            Console.WriteLine($"\nCalculando distancias con Flor Setosa...");

            var prediccion = prediceModelo.Predict(InstanciaIris.Setosa);
            Console.WriteLine($"Cluster analizado: {prediccion.PredecidoClusterId}");
            Console.WriteLine($"Distancias: {string.Join(" / ", prediccion.Distancias)}");

            Console.WriteLine($"\nCalculando distancias con Flor Versicolor...");
            var prediccion2 = prediceModelo.Predict(InstanciaIris.Versicolor);
            Console.WriteLine($"Cluster analizado: {prediccion2.PredecidoClusterId}");
            Console.WriteLine($"Distancias: {string.Join(" / ", prediccion2.Distancias)}");

            Console.WriteLine($"\nCalculando distancias con Flor Virginica...");
            var prediccion3 = prediceModelo.Predict(InstanciaIris.Virginica);
            Console.WriteLine($"Cluster analizado: {prediccion3.PredecidoClusterId}");
            Console.WriteLine($"Distancias: {string.Join(" / ", prediccion3.Distancias)}");


            Console.WriteLine($"\nPrograma finalizado con exito! ");
            Console.WriteLine("Presione Enter para salir...");

            //la distancia más pequeña es que tan cerca setosa del cluster analizado
            Console.ReadLine();

        }
    }
}
