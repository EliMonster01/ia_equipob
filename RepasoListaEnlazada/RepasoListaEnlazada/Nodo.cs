﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoListaEnlazada
{
    public class Nodo
    {
        public string Nombre;
        public int Numero;

        public Nodo Siguiente = null;

        public Nodo(string Nombre, int Numero)
        {
            this.Nombre = Nombre;
            this.Numero = Numero;
        }

    }
}