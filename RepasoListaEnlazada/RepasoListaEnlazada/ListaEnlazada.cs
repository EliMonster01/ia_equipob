﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoListaEnlazada
{
    class ListaEnlazada
    {
        public Nodo raiz = null;

        public void AgregarNodo(Nodo nodo_nuevo)
        {
            if (raiz == null)  //Si se encuentra vacío se agrega el nuevo nodo como raíz
            {
                raiz = nodo_nuevo;
            }
            else
            {
                Nodo nodo_aux = raiz; //Sino está vacio usamos un nodo auxiliar para determinar cuál es el último nodo
                                      //Igualando el nodo auxiliar con la raiz 
                while (nodo_aux.Siguiente != null) //Mientras el siguiente del nodo auxiliar (raiz), no sea el último 
                {
                    nodo_aux = nodo_aux.Siguiente;
                    nodo_aux.Siguiente = nodo_nuevo; //Entonces el nodo auxiliar se convierte en el último 
                }
            }
        }

        public string GetDatosLista()
        {
            Nodo auxiliar;
            auxiliar = raiz;
            string datos = "";
            do
            {
                datos += auxiliar.Nombre + " " + auxiliar.Numero + "/n";
                auxiliar = auxiliar.Siguiente;

            } while (auxiliar != null);

            return datos;
        }
    }
}
